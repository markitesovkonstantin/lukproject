using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleAppFib;

namespace UnitTestFib
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Addition_10_55returned()
        {
            //arrange
            int x = 10;
            int expected = 55;

            //act
            Fibonachi _fibonachi = new Fibonachi();
            int actual = _fibonachi.Addition(x);


            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Addition_11_89returned()
        {
            //arrange
            int x = 11;
            int expected = 89;

            //act
            Fibonachi _fibonachi = new Fibonachi();
            int actual = _fibonachi.Addition(x);


            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Addition_12_144returned()
        {
            //arrange
            int x = 12;
            int expected = 144;

            //act
            Fibonachi _fibonachi = new Fibonachi();
            int actual = _fibonachi.Addition(x);


            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
