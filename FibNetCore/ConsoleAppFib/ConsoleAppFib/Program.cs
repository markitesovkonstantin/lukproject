﻿using System;

namespace ConsoleAppFib
{
    class Program
    {
        static void Main(string[] args)
        {
            Fibonachi fibonachi = new Fibonachi();
            Console.WriteLine(fibonachi.Addition(10));
            Console.ReadKey();
        }
    }

    public class Fibonachi
    {
        public int Addition(int _fibInteger)
        {
            if ((_fibInteger == 0 || _fibInteger == 1))
                return _fibInteger;
            else
                return Addition(_fibInteger - 1) + Addition(_fibInteger - 2);
        }
    }
}
